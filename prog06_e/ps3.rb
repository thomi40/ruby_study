require './game.rb'
require './play.rb'
require './playstation.rb'

class PS3 < Game
  include Play
  include Playstation
end
