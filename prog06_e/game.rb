# スーパークラス(Game)
# initialize,play_gameは継承される
class Game
  def initialize(maker, cpu, version)
    @maker = maker
    @cpu = cpu
    @version = version
  end
end
