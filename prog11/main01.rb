require 'active_record'
require './user.rb'
require './init.rb'

puts "全体の平均 => #{User.average(:age)}"
puts "男性の平均 => #{User.group(:gender).average(:age)[0]}"
puts "女性の平均 => #{User.group(:gender).average(:age)[1]}"
