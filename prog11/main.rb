require 'active_record'
require './user.rb'
require './init.rb'

=begin
User.select('age').each do |user|
  puts user.age
end
=end

man_sum = 0
woman_sum = 0
man_count = 0
woman_count = 0

age_ave = (User.pluck(:age).inject(:+) / User.pluck(:age).size.to_f)

User.pluck('gender').each_with_index do |elem, i|
  if elem == 0
    man_sum   += User.pluck(:age)[i]
    man_count += 1
  else
    woman_sum   += User.pluck(:age)[i]
    woman_count += 1
  end
end

puts "全体の平均 => #{age_ave}"
puts "男性の平均 => #{man_sum.to_f / man_count}"
puts "女性の平均 => #{woman_sum.to_f / woman_count}"
