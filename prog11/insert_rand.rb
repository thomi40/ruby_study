require './user.rb'
require './init.rb'

max = ARGV[0]

(1..max.to_i).each do |_i|
  user = User.new(name: (0...5).map{ (97 + rand(26)).chr }.join, gender: rand(0..1), age: rand(0..100))
  user.save
end

#test02
# マルチプルインサート
# http://itpro.nikkeibp.co.jp/article/COLUMN/20060227/230902/?rt=nocnt