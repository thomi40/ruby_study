# スーパークラス(Game)
# initialize,play_gameは継承される
class Game
  @@machine_name = 'test' #サブクラスで初期化の必要
  def initialize(maker, cpu, version)
    @maker = maker
    @cpu = cpu
    @version = version
  end
  
  def play_game
    puts "welcome, #{@@machine_name}"
  end
end
