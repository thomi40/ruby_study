require './ps3.rb'

game1 = PS3.new('sony', 'new', 5.0)
game2 = PS3.new('sce', 'old', 4.0)

# play_gameメソッドはモジュール関数である
game1.play_game('PlayStation3')
game2.play_game('PlayStation2')

