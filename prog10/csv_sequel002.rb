require 'sequel'

DB = Sequel.connect('mysql://root:@localhost/csv', encoding: 'utf8')

rows = DB.fetch('SELECT AVG(age) as ave_age FROM age')
puts "全体の平均 => #{rows.first[:ave_age].to_f}"

gender_average_sql = 'SELECT AVG(age) as ave_age FROM age WHERE gender = ?'

man_rows = DB.fetch(gender_average_sql, 0)
woman_rows = DB.fetch(gender_average_sql, 1)

puts "男性の平均 => #{man_rows.first[:ave_age].to_f}"
puts "女性の平均 => #{woman_rows.first[:ave_age].to_f}"
