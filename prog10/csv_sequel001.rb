# 自作 rubyでコーディング
require 'sequel'

DB = Sequel.connect('mysql://root:@localhost/csv', encoding: 'utf8')

csv_all = DB[:age]
#csv_all = csv_gender.all

age_all = 0
age_man = 0
age_woman = 0
count_all = 0
count_man = 0


DB[:age].each do |row|
  age_all += row[:age]
  count_all += 1
end

csv_man = csv_all.where(:gender => '0')
csv_man.each do |row|
  age_man += row[:age]
  count_man += 1
end

csv_woman = csv_all.where(:gender => '1')
csv_woman.each do |row|
  age_woman += row[:age]
end

puts "男性の平均 => #{age_man / count_man.to_f}"
puts "女性の平均 => #{age_woman / (count_all-count_man).to_f}"
puts "全員の平均 => #{age_all / count_all.to_f}"

