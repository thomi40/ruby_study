# 自作プログラム

require 'csv'

table = CSV.table('data.csv')

table.headers #  [:ID, :name, :gender, :age]

age_sum = 0.0
man_sum = 0.0
woman_sum = 0.0

table[:age].each do |i|
  age_sum += i
end

count = 0
man_count = 0
woman_count = 0


table[:gender].each do |j|
  if j == 0
    man_sum   += table[:age][count]
    man_count += 1
  else
    woman_sum   += table[:age][count]
    woman_count += 1
  end
  count += 1 #行数のカウント
end

puts "全体の平均 => #{age_sum/table[:age].length}"
puts "男性の平均 => #{man_sum/man_count}"
puts "女性の平均 => #{woman_sum/woman_count}"
