require 'csv'

table = CSV.table('data.csv')

table.headers #  [:ID, :name, :gender, :age]

man_sum = 0
woman_sum = 0
man_count = 0
woman_count = 0

age_ave = (table[:age].inject(:+) / table[:age].size.to_f)

table[:gender].each_with_index do |elem, i|
  if elem == 0
    man_sum   += table[:age][i]
    man_count += 1
  else
    woman_sum   += table[:age][i]
    woman_count += 1
  end
end

puts "男性の平均 => #{man_sum.to_f / man_count}"
puts "女性の平均 => #{woman_sum.to_f / woman_count}"
puts "全体の平均 => #{age_ave}"
