class Human

  def initialize(id, age, jender, name)
    @id = id
    @age = age
    @jender = jender
    @name = name
  end
end

human1 = Human.new(1, 40, 'woman', 'tomo')
human2 = Human.new(2, 20, 'man', 'noda')
human3 = Human.new(3, 21, 'man', 'ishimaru')

puts "#{human1.id},#{human1.age},#{human1.jender},#{human1.name}"
puts "#{human2.id},#{human2.age},#{human2.jender},#{human2.name}"
puts "#{human3.id},#{human3.age},#{human3.jender},#{human3.name}"
  
