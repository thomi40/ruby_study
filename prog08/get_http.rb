require 'httpclient'

# レスポンスのクラスが返ってくる
# response はメッセージクラスのインスタンス
# HTTPClient.get はスタティックメソッド
response = HTTPClient.get(ARGV[0])
puts response.content