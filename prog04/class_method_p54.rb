class Human
  @@country = 'Japan'
  attr_accessor :name
  def initialize(name)
    @name = name
  end

  def welcome
    puts 'welcome' + ' ' + @@country + ' ' + @name
  end
end

human1 = Human.new('ishimaru')
human1.welcome
