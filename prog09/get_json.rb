require 'httpclient'
require 'json'

# レスポンスのクラスが返ってくる
# response はメッセージクラスのインスタンス
# HTTPClient.get はスタティックメソッド
json_response = HTTPClient.get(ARGV[0])
# puts json_response.content

ruby_response = JSON.load(json_response.content)

# p ruby_response

v10 = ruby_response[9]
# puts v10

# puts v10['title']

ruby_response.each do |v|
  puts v['title']
end